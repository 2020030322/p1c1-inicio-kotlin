package com.example.p1c1_inicio_kotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var btnSaludar: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnSalir: Button;
    private lateinit var txtNombre: EditText;
    private lateinit var lblSaludo: TextView;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //BOTONES
        btnSaludar = findViewById(R.id.btnSaludar) as Button;
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button;
        btnSalir = findViewById(R.id.btnSalir) as Button;
        //LABELS Y TEXTOS
        txtNombre = findViewById(R.id.txtNombre) as EditText;
        lblSaludo = findViewById(R.id.lblSaludo) as TextView;
        //codigo de lo que hara el boton
        btnSaludar.setOnClickListener({
            var str : String;
            if(txtNombre.text.toString().contentEquals("")){
                Toast.makeText(applicationContext, "Favor de ingresar un nombre", Toast.LENGTH_LONG).show();
            }else{
                val txtSaludar = txtNombre.text.toString()
                lblSaludo.text = "Hola $txtSaludar Como Estas?"
            }
        })
        btnLimpiar.setOnClickListener({
            txtNombre.setText("");
            lblSaludo.setText(".... --- .-.. .-  / .-- . -. .- ...")
        })
        btnSalir.setOnClickListener({
            finish()
        })
    }
}

